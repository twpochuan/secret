import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:secret/models/person_model.dart';
import 'package:zefyr/zefyr.dart';

class CardModel extends Equatable {
  final String id;
  final PersonModel receiver;
  final PersonModel creator;
  final String title;
  final NotusDocument body;
  final int type;
  final Timestamp createdAt;

  CardModel({
    @required this.id,
    @required this.creator,
    @required this.receiver,
    @required this.title,
    @required this.body,
    @required this.type,
    @required this.createdAt
  }): super([id, creator, receiver, title, body, type, createdAt]);


  factory CardModel.fromJson(Map<String, dynamic> source) {
    String bodyJsonStr = source['body'] as String;
    NotusDocument body = NotusDocument.fromJson(
        json.decode(bodyJsonStr) as List);

    return CardModel(
      id: source['id'] as String,
      receiver: PersonModel.fromJson(source['receiver']),
      creator: PersonModel.fromJson(source['creator']),
      title: source['title'] as String,
      body: body,
      type: source['type'] as int,
      createdAt: source['createdAt'] as Timestamp,
    );
  }

  factory CardModel.fromPartialJson(Map<String, dynamic> source, {
    @required PersonModel receiver,
    @required PersonModel creator,
  }) {
    String bodyJsonStr = source['body'] as String;
    NotusDocument body = NotusDocument.fromJson(
        json.decode(bodyJsonStr) as List);

    return CardModel(
      id: source['id'] as String,
      receiver: receiver,
      creator: creator,
      title: source['title'] as String,
      body: body,
      type: source['type'] as int,
      createdAt: source['createdAt'] as Timestamp,
    );
  }
}