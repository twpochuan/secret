import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:secret/models/person_model.dart';

class CardRefModel extends Equatable {
  final String id;
  final PersonModel receiver;
  final PersonModel creator;
  final String title;
  final int type;
  final Timestamp createdAt;

  CardRefModel({
    @required this.id,
    @required this.creator,
    @required this.receiver,
    @required this.title,
    @required this.type,
    @required this.createdAt
  }): super([id, creator, receiver, title, type, createdAt]);

  factory CardRefModel.fromJson(Map<String, dynamic> source) {
    return CardRefModel(
      id: source['id'] as String,
      receiver: PersonModel.fromJson(source['receiver']),
      creator: PersonModel.fromJson(source['creator']),
      title: source['title'] as String,
      type: source['type'] as int,
      createdAt: source['createdAt'] as Timestamp,
    );
  }

  factory CardRefModel.fromPartialJson(Map<String, dynamic> source, {
    @required PersonModel receiver,
    @required PersonModel creator,
  }) {

    return CardRefModel(
      id: source['id'] as String,
      receiver: receiver,
      creator: creator,
      title: source['title'] as String,
      type: source['type'] as int,
      createdAt: source['createdAt'] as Timestamp,
    );
  }
}