import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class PersonModel extends Equatable {
  final String uid;
  final String name;


  PersonModel({@required this.uid, @required this.name})
      : super([uid, name]);

  factory PersonModel.fromJson(Map<String, dynamic> source) {
    return PersonModel(
      uid: source['uid'] as String,
      name: source['name'] as String,
    );
  }

}