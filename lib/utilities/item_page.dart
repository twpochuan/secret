import 'dart:collection';

/// A page of items fetched from network.
///
/// This mimics a paginated web API response, where you don't get results
/// one by one but in batches.
class ItemPage<T> {
  final List<T> _items;

  final String startReference;
  final String endReference;

  ItemPage(this._items, this.startReference, this.endReference);

  int get count => _items.length;


  UnmodifiableListView<T> get items =>
      UnmodifiableListView<T>(_items);

}