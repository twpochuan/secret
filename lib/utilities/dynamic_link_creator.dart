import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:meta/meta.dart';

Future<String> createDynamicLink({
	@required String docId,
	@required String title,
	@required String description,
	@required String imageUrl}) async {
	final DynamicLinkParameters parameters = DynamicLinkParameters(
		uriPrefix: 'https://mydear.page.link',
		link: Uri.parse('https://ytnpc.club/?card=' + docId),
		androidParameters: AndroidParameters(
			packageName: 'club.ytnpc.secret',
		),
		socialMetaTagParameters: SocialMetaTagParameters(
			title: title,
			description: description,
			imageUrl: Uri.parse(imageUrl),
		),
	);

	final ShortDynamicLink shortDynamicLink = await parameters.buildShortLink();
	return shortDynamicLink.shortUrl.toString();
}
