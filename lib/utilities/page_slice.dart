import 'dart:math';

import 'package:secret/utilities/item_page.dart';


/// A slice of the catalog provided to an infinite-scrolling [ListView].
///
/// A [CatalogSlice] is a convenience class backed by an arbitrary
/// number of [CatalogPage]s. Most of the time, there is more than one
/// [CatalogPage] in memory — but the view code should _not_ need to worry about
/// that.
class PageSlice<T> {
  final List<ItemPage<T>> _pages;

  final String startReference;
  final String endReference;

  /// Whether or not this slice is the end of the page.
  ///
  /// Currently always `true` as our catalog is infinite.
  final bool hasNext;

  PageSlice(this._pages, this.hasNext)
      : startReference = _pages.first.startReference,
        endReference = _pages.last.endReference;

  const PageSlice.empty()
      : _pages = const [],
        startReference = null,
        endReference = null,
        hasNext = true;


  /// Returns the product at [index], or `null` if data isn't loaded yet.
//  T elementAt(int index) {
//    for (final page in _pages) {
//      if (index >= page.startIndex && index <= page.endIndex) {
//        return page.products[index - page.startIndex];
//      }
//    }
//    return null;
//  }
}
