import 'package:meta/meta.dart';

class MemoryCache {
  static final MemoryCache instance = MemoryCache._internal();
  final Map<String, dynamic> _cache = <String, dynamic>{};

  MemoryCache._internal();

  void add(String key, dynamic value) =>
      _cache[key] = value;

  dynamic get(String key) => _cache[key];

  dynamic remove(String key) => _cache.remove(key);

  bool containsKey(String key) => _cache.containsKey(key);

  void clear() => _cache.clear();

}
