import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class AuthState extends Equatable {
  AuthState([Iterable props]) : super(props);
}

class AuthUninitialized extends AuthState {
  @override
  String toString() => 'AuthUninitialized';
}

class AuthInitialized extends AuthState {
  final bool isLoading;
  final FirebaseUser user;

  AuthInitialized({
    @required this.isLoading,
    @required this.user,
  }) : super([isLoading, user]);

  bool get isAuthenticated => user != null;

  factory AuthInitialized.authenticated({@required FirebaseUser user}) {
    return AuthInitialized(
      isLoading: false,
      user: user,
    );
  }

  factory AuthInitialized.unauthenticated() {
    return AuthInitialized(
      isLoading: false,
      user: null,
    );
  }

  @override
  String toString() =>
      'AuthInitialized { isLoading: $isLoading, user: $user }';
}