import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:bloc/bloc.dart';
import 'package:secret/blocs/auth/auth_event.dart';
import 'package:secret/blocs/auth/auth_state.dart';
import 'package:secret/utilities/memory_cache.dart';


class AuthBloc extends Bloc<AuthEvent, AuthState> {
  static final AuthBloc instance = AuthBloc._internal();
  final FirebaseAuth firebaseAuth;
  StreamSubscription<FirebaseUser> _firebaseUserSub;

  AuthBloc._internal()
      : firebaseAuth = FirebaseAuth.instance {

    dispatch(StatusUpdatedAuthEvent());

    _firebaseUserSub = firebaseAuth.onAuthStateChanged.listen((user) {
      dispatch(StatusUpdatedAuthEvent());
    });
  }

  @override
  void dispose() {
    if (_firebaseUserSub != null) {
      _firebaseUserSub.cancel();
    }

    super.dispose();
  }

  FirebaseUser get currentUser {
    if (currentState is AuthInitialized) {
      // refresh token
      firebaseAuth.currentUser();
      return (currentState as AuthInitialized).user;
    }

    return null;
  }

  @override
  AuthState get initialState => AuthUninitialized();

  @override
  Stream<AuthState> mapEventToState(AuthState currentState, AuthEvent event) async* {
    if (event is AppStartAuthEvent || event is StatusUpdatedAuthEvent) {
      final FirebaseUser user = await firebaseAuth.currentUser();

      if (user != null) {
        yield AuthInitialized.authenticated(user: user);
      } else {
        yield AuthInitialized.unauthenticated();
      }
    }

    if (event is LogoutAuthEvent) {
      yield AuthInitialized(user: null, isLoading: true);
      await firebaseAuth.signOut();
      MemoryCache.instance.clear();
//      yield AuthInitialized.unauthenticated();
    }
  }
}
