import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class AuthEvent extends Equatable {
  AuthEvent([Iterable props]) : super(props);
}

class AppStartAuthEvent extends AuthEvent {
  @override
  String toString() => 'AppStart';
}

class StatusUpdatedAuthEvent extends AuthEvent {
  @override
  String toString() => 'Check Status';
}

class LogoutAuthEvent extends AuthEvent {
  @override
  String toString() => 'Logout';
}