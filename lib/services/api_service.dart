import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:secret/blocs/auth/auth_bloc.dart';
import 'package:secret/models/person_model.dart';
import 'package:secret/utilities/memory_cache.dart';

class ApiService {
  static final ApiService instance = ApiService._internal();
  final MemoryCache _cache = MemoryCache.instance;

  ApiService._internal();

  Future<PersonModel> getContact() async {
    final String receiverKey = 'receiver';
    if (_cache.containsKey(receiverKey)) {
      return _cache.get(receiverKey);
    }

    String uid = AuthBloc.instance.currentUser?.uid;
    DocumentSnapshot contactRecords = await Firestore.instance
        .collection('users').document(uid)
        .collection('contacts').document('records')
        .get();
    String receiverId = contactRecords?.data?.keys?.first;
    PersonModel receiver = await getPerson(receiverId);

    _cache.add(receiverKey, receiver);

    return receiver;
  }

  Future<PersonModel> getPerson(String uid) async {
    if (_cache.containsKey(uid)) {
      return _cache.get(uid);
    }

    DocumentSnapshot docSnapshot = await Firestore.instance
        .collection('users').document(uid)
        .get();

    String name = docSnapshot?.data['name'];
    PersonModel person = PersonModel(
      uid: uid,
      name: name,
    );

    _cache.add(uid, person);

    return person;
  }
}