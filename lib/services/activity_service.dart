import 'dart:io';

import 'package:flutter/services.dart';

const platform = const MethodChannel('secret.ytnpc.club');

Future<void> moveTaskToBack() async {
  if (!Platform.isAndroid) {
    return;
  }

  try {
    final bool result = await platform.invokeMethod('moveTaskToBack');
  } on PlatformException catch (e) {
    print("Failed to move task to background: '${e.message}'.");
  }
}