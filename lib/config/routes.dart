import 'package:fluro/fluro.dart';
import 'package:secret/config/route_handlers.dart';

class Routes {
  static final String root = "/";
  static final String login = "/login";
  static final String cardCreation = "/card_creation";
  static final String cardView = "/card/:cardId";

  static void configureRoutes(Router router) {
    router.notFoundHandler = notFoundHandler;
    router.define(root, handler: rootHandler);
    router.define(login, handler: loginHandler);
    router.define(cardCreation, handler: cardCreationHandler);
    router.define(cardView, handler: cardViewHandler);
  }
}