import 'package:fluro/fluro.dart';
import 'package:secret/config/routes.dart';

class Application {
  static final Application instance = Application._internal();

  final Router router;

  Application._internal(): router = Router() {
    Routes.configureRoutes(router);
  }
}