import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:secret/models/person_model.dart';
import 'package:secret/pages/card/creation/card_creation_page.dart';
import 'package:secret/pages/card/view/card_view_page.dart';
import 'package:secret/pages/home/home_page.dart';
import 'package:secret/pages/login/login_page.dart';
import 'package:secret/config/routes.dart';


final Handler notFoundHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return HomePage();
    });

final Handler rootHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {

      return HomePage();
  }
);

final Handler loginHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      String redirectTo = params['redirectTo']?.first ?? Routes.root;

      return LoginPage(redirectTo: redirectTo);
    }
);

final Handler cardCreationHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      String receiverId = params['receiverId']?.first;
      String receiverName = params['receiverName']?.first;

      return CardCreationPage(receiver: PersonModel(
        uid: receiverId,
        name: receiverName,
      ));
    }
);

final Handler cardViewHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      String cardId = params['cardId']?.first;

      return CardViewPage(
        cardId: cardId,
        fromDeepLink: params['fromDeepLink'] != null,
      );
    }
);
