import 'dart:io';
import 'dart:convert';
import 'package:convert/convert.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:crypto/crypto.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:notus/notus.dart';
import 'package:secret/blocs/auth/auth_bloc.dart';
import 'package:secret/models/person_model.dart';
import 'package:secret/pages/card/creation/card_creation_event.dart';
import 'package:secret/pages/card/creation/card_creation_state.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:secret/services/api_service.dart';
import 'package:secret/utilities/dynamic_link_creator.dart';




class CardCreationBloc extends Bloc<CardCreationEvent, CardCreationState> {

  @override
  get initialState => CardCreationState.initial();

  @override
  Stream<CardCreationState> mapEventToState(CardCreationState currentState,
      CardCreationEvent event) async* {
    if (event is CardCreationInitializedEvent) {
      yield CardCreationState.initialized();
    }


    if (event is CardCreationTitleOnChangedEvent) {
      yield CardCreationState(
        isInitial: false,
        isLoading: currentState.isLoading,
        isDone: false,
        titleFormatError: '',
        bodyFormatError: currentState.bodyFormatError,
        error: '',
        deepLink: currentState.deepLink,
        cardId: null,
      );
    }

    if (event is CardCreationBodyOnChangedEvent) {
      yield CardCreationState(
        isInitial: false,
        isLoading: currentState.isLoading,
        isDone: false,
        titleFormatError: currentState.titleFormatError,
        bodyFormatError: '',
        error: '',
        deepLink: currentState.deepLink,
        cardId: null,
      );
    }

    if (event is CardCreationSubmitRequestEvent) {
      bool hasError = false;
      String titleFormatErrorMsg = '';
      String bodyFormatErrorMsg = '';


      if (event.title == null || event.title.isEmpty) {
        hasError = true;
        titleFormatErrorMsg = '標題不能空白哦';
      }

      if (event.body == null || event.body.toPlainText().isEmpty) {
        hasError = true;
        bodyFormatErrorMsg = '寫點什麼唄...';
      }

      if (hasError) {
        yield CardCreationState.failure(
            titleFormatError: titleFormatErrorMsg,
            bodyFormatError: bodyFormatErrorMsg
        );
      } else {
        yield CardCreationState.loading();

        try {
          String bodyText = await _processBodyDoc(event.body);

          FirebaseUser currentUser = AuthBloc.instance.currentUser;
          String uid = currentUser.uid;

          // Create document on /cards/
          DocumentReference cardRef = await Firestore.instance.collection('cards').add({
            'creatorId': uid,
            'receiverId': event.receiverId,
            'title':  event.title,
            'body': bodyText,
            'type': 1,
            'createdAt': FieldValue.serverTimestamp(),
          });

          // Create a card reference to the creator
          await Firestore.instance.collection('users')
              .document(uid).collection('cards').document(cardRef.documentID)
              .setData({
            'creatorId': uid,
            'receiverId': event.receiverId,
            'title': event.title,
            'type': 1,
            'createdAt': FieldValue.serverTimestamp(),
          });

          PersonModel creator = await ApiService.instance
              .getPerson(uid);
          PersonModel receiver = await ApiService.instance
              .getPerson(event.receiverId);

          String deepLink = await createDynamicLink(
            docId: cardRef.documentID,
            title: '${creator.name}給${receiver.name}的一封信',
            description: event.title,
            imageUrl: 'https://firebasestorage.googleapis.com/'
                'v0/b/dear-961fc.appspot.com/o/deep_link_image.jpg'
                '?alt=media&token=2464afa2-0b2a-48d7-afad-be0bf114c8fe',
          );

          yield CardCreationState.success(
              cardId: cardRef.documentID,
              deepLink: deepLink,
          );
        } catch (e) {
          print(e);
          yield CardCreationState.failure(error: e.toString());
        }
      }
    }
  }

  Future<String> _processBodyDoc(NotusDocument doc) async {
    String bodyText = json.encode(doc);
    RegExp exp = RegExp(r'file:///[^"]+([.][^"]+)');
    Iterable<Match> matches = exp.allMatches(bodyText);

    if (matches.isEmpty) {
      return bodyText;
    }

    StorageReference storageRef = FirebaseStorage.instance.ref();
    List<Future<StorageTaskSnapshot>> uploadTasks =
      <Future<StorageTaskSnapshot>>[];

    final String uid = AuthBloc.instance.currentUser.uid;

    for (Match m in matches) {
      String filePath = m.group(0).replaceFirst('file:///', '');
      String type = m.group(1);
      String hashFilename = _generateMd5(
          uid + filePath + DateTime.now().millisecondsSinceEpoch.toString()
      );
      hashFilename += type;


      Directory tempDir = await getTemporaryDirectory();

      File compressedFile = await FlutterImageCompress.compressAndGetFile(
        filePath, '${tempDir.path}/$hashFilename',
        quality: 93,
      );

      StorageReference ref = storageRef.child('users')
          .child(uid).child('uploads').child(hashFilename);
      uploadTasks.add(ref.putFile(compressedFile).onComplete);
    }

    List<StorageTaskSnapshot> snapshots = await Future.wait(uploadTasks);
    List<dynamic> urls = await Future.wait(
        snapshots.map((snapshot) => snapshot.ref.getDownloadURL())
    );

    int index = 0;
    bodyText = bodyText.replaceAllMapped(exp, (match) {
      print('${match.group(0)} : ${urls[index]}');
      return urls[index++];
    });

    print(bodyText);

    return bodyText;
  }

  String _generateMd5(String data) {
    var content = Utf8Encoder().convert(data);
    var digest = md5.convert(content);
    return hex.encode(digest.bytes);
  }
}
