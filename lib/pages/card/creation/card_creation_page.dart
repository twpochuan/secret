import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:secret/blocs/auth/auth_bloc.dart';
import 'package:secret/blocs/auth/auth_event.dart';
import 'package:secret/blocs/auth/auth_state.dart';
import 'package:secret/config/application.dart';
import 'package:secret/models/person_model.dart';
import 'package:secret/pages/card/creation/card_creation_bloc.dart';
import 'package:secret/pages/card/creation/card_creation_event.dart';
import 'package:secret/pages/card/creation/card_creation_state.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zefyr/zefyr.dart';



class CardCreationPage extends StatefulWidget {
  final PersonModel receiver;

  CardCreationPage({@required this.receiver});

  @override
  State<StatefulWidget> createState() => CardCreationPageState();

}

class CardCreationPageState extends State<CardCreationPage> {
  final CardCreationBloc _cardCreationBloc = CardCreationBloc();
  final FocusNode _focusNode = FocusNode();
  TextEditingController _titleTextController;
  ZefyrController _bodyTextController;
  String _lastTitle;
  String _lastBody;


  bool _isSubmitSucceeded(CardCreationState state) => state.isDone
      && state.deepLink != null;
  bool _isSubmitFailed(CardCreationState state) => state.error.isNotEmpty;


  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthEvent, AuthState>(
        bloc: AuthBloc.instance,
        builder: (BuildContext context, AuthState state) {
          if (state is AuthInitialized) {
            if (state.isAuthenticated) {
              return _buildCardCreationPage(context: context);
            }
            _onWidgetDidBuild(() {
              Application.instance.router.navigateTo(
                context, 'login',
                clearStack: true,
              );
            });
          }

          return _buildLoadingPage(context: context);
        }
    );
  }

  Widget _buildLoadingPage({@required BuildContext context}) {
    return Scaffold(
      appBar: AppBar(
        title: Text('給${widget.receiver.name}的一封信'),
      ),
      body: Container(
        alignment: Alignment.center,
        child: SpinKitFadingCube (
            color: Theme.of(context).primaryColor,
            size: 40
        ),
      ),
    );

  }

  Widget _buildCardCreationPage({@required BuildContext context}) {
    return BlocBuilder<CardCreationEvent, CardCreationState> (
        bloc: _cardCreationBloc,
        builder: (BuildContext context, CardCreationState state) {
          if (state.isInitial) {
            _initialize(
              context: context,
              bloc: _cardCreationBloc,
              state: state,
            );
            return _buildLoadingPage(context: context);
          }

          if (_isSubmitSucceeded(state)) {
            _submitSucceeded(state);
          }

          return  Scaffold(
            appBar: AppBar(
              title: Text('給${widget.receiver.name}的一封信'),
              actions: <Widget>[
                _buildSubmitIconButton(
                  context: context,
                  bloc: _cardCreationBloc,
                  state: state,
                ),
              ],
            ),
            body: _buildScaffoldBody(
                context: context,
                bloc: _cardCreationBloc,
                state: state
            ),
          );
        }
    );
  }

  Widget _buildScaffoldBody({
    @required BuildContext context,
    @required CardCreationBloc bloc,
    @required CardCreationState state}) {

    return Builder(
        builder: (context) {
          if (_isSubmitFailed(state)) {
            _onWidgetDidBuild(() {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                      '出錯囉！\n錯誤: ${state.error}',
                  ),
                  backgroundColor: Colors.red,
                ),
              );
            });
          }

          return ZefyrScaffold(
            child: _buildForm(
              context: context,
              bloc: bloc,
              state: state,
            ),
          );
        }
    );
  }

  Widget _buildForm({
    @required BuildContext context,
    @required CardCreationBloc bloc,
    @required CardCreationState state}) {
      return Form(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 20,
          ),
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5),
                child: _buildTitleTextFormField(
                  context: context,
                  bloc: bloc,
                  state: state,
                ),
              ),
              Expanded(
                flex: 1,
                child: _buildBodyTextFormField(
                  context: context,
                  bloc: bloc,
                  state: state,
                ),
              ),
            ],
          ),
        ),
      );
  }

  Widget _buildTitleTextFormField({
    @required BuildContext context,
    @required CardCreationBloc bloc,
    @required CardCreationState state}) {
    return TextFormField(
        enabled: !state.isLoading,
        keyboardType: TextInputType.text,
        controller: _titleTextController,
        decoration: InputDecoration(
          hintText: '標題',
          labelText: '標題',
          errorText: _errorText(state.titleFormatError),
        ),
      );
  }

  Widget _buildBodyTextFormField({
    @required BuildContext context,
    @required CardCreationBloc bloc,
    @required CardCreationState state}) {

    final theme = ZefyrThemeData.fallback(context);

    return ZefyrTheme(
      data: theme,
      child: ZefyrField(
        decoration: InputDecoration(
          hintText: '寫點什麼唄',
          labelText: '內容',
          errorText: _errorText(state.bodyFormatError),
        ),
        controller: _bodyTextController,
        focusNode: _focusNode,
        autofocus: false,
        imageDelegate: CustomImageDelegate(),
        physics: ClampingScrollPhysics(),
      )
    );
  }

  Widget _buildSubmitIconButton({
    @required BuildContext context,
    @required CardCreationBloc bloc,
    @required CardCreationState state}) {
    return state.isLoading
        ? Padding(
          padding: EdgeInsets.all(10),
          child: SpinKitWave(
            color: Colors.white,
            size: 40
          ),
        ) : IconButton(
            icon: Icon(Icons.send),
            onPressed: () {
              _submit();
            }
          );
  }

  void _submit() {
    _cardCreationBloc.dispatch(
        CardCreationSubmitRequestEvent(
          receiverId: widget.receiver.uid,
          title: _titleTextController.text,
          body: _bodyTextController.document,
        )
    );
  }

  Future<void> _submitSucceeded(CardCreationState state) async {
    _clearDraft();

    final RenderBox box = context.findRenderObject();
    await Share.share(state.deepLink,
        sharePositionOrigin:
        box.localToGlobal(Offset.zero) &
        box.size
    );

    Application.instance.router.navigateTo(
      context, '/card/${state.cardId}',
      replace: true,
    );
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  String _errorText(String errorMessage) {
    return errorMessage.isNotEmpty ? errorMessage : null;
  }

  String _docToString(NotusDocument doc) {
    return json.encode(doc);
  }

  Future<void> _saveDraft() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList('draft', [
      _lastTitle,
      _lastBody,
    ]);
  }

  Future<void> _initialize({
    @required BuildContext context,
    @required CardCreationBloc bloc,
    @required CardCreationState state,
  }) async {
    await _loadDraft();

    _titleTextController = (_lastTitle != null && _lastTitle.isNotEmpty)
        ? TextEditingController(text: _lastTitle)
        : TextEditingController();

    NotusDocument bodyDoc = (_lastBody != null && _lastBody.isNotEmpty)
        ? NotusDocument.fromJson(json.decode(_lastBody) as List)
        : NotusDocument();
    _bodyTextController = ZefyrController(bodyDoc);

    _titleTextController.addListener(() {
      String title = _titleTextController.text;
      if (_lastTitle != title) {
        _cardCreationBloc.dispatch(CardCreationTitleOnChangedEvent());
        _lastTitle = title;
        _saveDraft();
      }

    });

    _bodyTextController.addListener(() {
      String body = _docToString(_bodyTextController.document);
      if (_lastBody != body) {
        _cardCreationBloc.dispatch(CardCreationBodyOnChangedEvent());
        _lastBody = body;
        _saveDraft();
      }
    });

    bloc.dispatch(CardCreationInitializedEvent());
  }


  Future<void> _loadDraft() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> draft = prefs.getStringList('draft');
    if (draft == null || draft.length != 2) {
      return;
    }

    _lastTitle = draft.first;
    _lastBody = draft.last;
  }

  Future<void> _clearDraft() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('draft');
  }



}

class CustomImageDelegate implements ZefyrImageDelegate<ImageSource> {
  @override
  Widget buildImage(BuildContext context, String imageSource) {
    // We use custom "asset" scheme to distinguish asset images from other files.
    if (imageSource.startsWith('asset://')) {
      return _buildImageView(
        context: context,
        provider: AssetImage(imageSource.replaceFirst('asset://', '')),
      );
    } else if (imageSource.startsWith('file://')) {
      final file = File.fromUri(Uri.parse(imageSource));
      final image = FileImage(file);

      return _buildImageView(
          context: context,
          provider: image
      );
    }

    return Container();
  }

  Widget _buildImageView({
    @required BuildContext context,
    @required ImageProvider provider,
  }) {
    return Container(
      alignment: Alignment.center,
      child: Container(
        width: 300,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          elevation: 2,
          child:Image(
            alignment: Alignment.center,
            image: provider,
          ),
        ),
      ),
    );
  }

  @override
  ImageSource get cameraSource => ImageSource.camera;

  @override
	ImageSource get gallerySource => ImageSource.gallery;

  @override
  Future<String> pickImage(ImageSource source) async {
		final file = await ImagePicker.pickImage(source: source);
		if (file == null) return null;
		return file.uri.toString();
  }
}

