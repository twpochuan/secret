import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:notus/notus.dart';

abstract class CardCreationEvent extends Equatable {
  CardCreationEvent([Iterable props]) : super(props);
}

class CardCreationTitleOnChangedEvent extends CardCreationEvent {

}
class CardCreationBodyOnChangedEvent extends CardCreationEvent {

}

class CardCreationInitializedEvent extends CardCreationEvent {

}

class CardCreationSubmitRequestEvent extends CardCreationEvent {
  final String receiverId;
  final String title;
  final NotusDocument body;

  CardCreationSubmitRequestEvent({
    @required this.receiverId,
    @required this.title,
    @required this.body
  }) :super([receiverId, title, body]);
}