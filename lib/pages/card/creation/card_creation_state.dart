import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

class CardCreationState extends Equatable {
  final bool isInitial;
  final bool isLoading;
  final String titleFormatError;
  final String bodyFormatError;
  final bool isDone;
  final String error;
  final String deepLink;
  final String cardId;

  CardCreationState({
    @required this.isInitial,
    @required this.isLoading,
    @required this.isDone,
    @required this.titleFormatError,
    @required this.bodyFormatError,
    @required this.error,
    @required this.deepLink,
    @required this.cardId,
  }) : super([isInitial, isLoading, isDone, titleFormatError, bodyFormatError, error, deepLink, cardId]);

  bool get isSubmitButtonEnabled {
    return !isInitial
        && !isLoading
        && !isDone
        && titleFormatError.isEmpty
        && bodyFormatError.isEmpty;
  }

  bool get hasError {
    return error.isNotEmpty
        && titleFormatError.isNotEmpty
        && bodyFormatError.isNotEmpty;
  }

  bool get isTitleFormatValid => titleFormatError.isEmpty;
  bool get isBodyFormatValid => bodyFormatError.isEmpty;

  factory CardCreationState.initial() {
    return CardCreationState(
      isInitial: true,
      isLoading: false,
      isDone: false,
      titleFormatError: '',
      bodyFormatError: '',
      error: '',
      deepLink: null,
      cardId: null,
    );
  }

  factory CardCreationState.initialized() {
    return CardCreationState(
      isInitial: false,
      isLoading: false,
      isDone: false,
      titleFormatError: '',
      bodyFormatError: '',
      error: '',
      deepLink: null,
      cardId: null,
    );
  }


  factory CardCreationState.loading() {
    return CardCreationState(
      isInitial: false,
      isLoading: true,
      isDone: false,
      titleFormatError: '',
      bodyFormatError: '',
      error: '',
      deepLink: null,
      cardId: null,
    );
  }

  factory CardCreationState.failure({String titleFormatError: '',
    String bodyFormatError: '', String error: ''}) {
    return CardCreationState(
      isInitial: false,
      isLoading: false,
      isDone: false,
      titleFormatError: titleFormatError,
      bodyFormatError: bodyFormatError,
      error: error,
      deepLink: null,
      cardId: null,
    );
  }

  factory CardCreationState.success({@required deepLink, @required cardId}) {
    return CardCreationState(
      isInitial: false,
      isLoading: false,
      isDone: true,
      titleFormatError: '',
      bodyFormatError: '',
      error: '',
      deepLink: deepLink,
      cardId: cardId,
    );
  }

  @override
  String toString() =>
      'CardCreationState {'
          'isInitial: $isInitial, '
          'isLoading: $isLoading, '
          'isDone: $isDone, '
          'titleFormatError: $titleFormatError, '
          'bodyFormatError: $bodyFormatError, '
          'error: $error, '
          'cardId: $cardId, '
          'deepLink: $deepLink '
          '}';

}