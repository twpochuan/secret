import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:secret/models/card_model.dart';

class CardViewState extends Equatable {
  final bool isInitial;
  final bool isLoading;
  final bool isLoadingDeepLink;
  final String loadingError;
  final String deepLinkError;
  final String deepLink;
  final CardModel cardModel;

  CardViewState({
    @required this.isInitial,
    @required this.isLoading,
    @required this.isLoadingDeepLink,
    @required this.loadingError,
    @required this.deepLinkError,
    @required this.deepLink,
    @required this.cardModel,
  }): super([isInitial, isLoading, isLoadingDeepLink, loadingError, deepLinkError, deepLink, cardModel]);

  bool get hasLoadingError => loadingError != null
      && loadingError.isNotEmpty;
  bool get hasDeepLinkError => deepLinkError != null
      && deepLinkError.isNotEmpty;

  factory CardViewState.initial() {
    return CardViewState(
      isInitial: true,
      isLoading: false,
      isLoadingDeepLink: false,
      loadingError: '',
      deepLinkError: '',
      deepLink: null,
      cardModel: null,
    );
  }

  factory CardViewState.basedOn({
    @required CardViewState previousState,
    bool isInitial,
    bool isLoading,
    bool isLoadingDeepLink,
    String loadingError,
    String deepLinkError,
    String deepLink,
    CardModel cardModel}) {

    return CardViewState(
      isInitial: isInitial ?? previousState.isInitial,
      isLoading: isLoading ?? previousState.isLoading,
      isLoadingDeepLink: isLoadingDeepLink ?? previousState.isLoadingDeepLink,
      loadingError: loadingError ?? previousState.loadingError,
      deepLinkError: deepLinkError ?? previousState.deepLinkError,
      deepLink: deepLink ?? previousState.deepLink,
      cardModel: cardModel ?? previousState.cardModel,
    );
  }


  factory CardViewState.loading() {
    return CardViewState(
      isInitial: false,
      isLoading: true,
      isLoadingDeepLink: false,
      loadingError: '',
      deepLinkError: '',
      deepLink: null,
      cardModel: null,
    );
  }

  factory CardViewState.loadingFailure({@required String loadingError}) {
    return CardViewState(
      isInitial: false,
      isLoading: false,
      isLoadingDeepLink: false,
      loadingError: loadingError,
      deepLinkError: '',
      deepLink: null,
      cardModel: null,
    );
  }

  factory CardViewState.cardModel({@required CardModel cardModel}) {
    return CardViewState(
      isInitial: false,
      isLoading: false,
      isLoadingDeepLink: false,
      loadingError: '',
      deepLinkError: '',
      deepLink: null,
      cardModel: cardModel,
    );
  }
}