import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:intl/intl.dart';

import 'package:secret/blocs/auth/auth_bloc.dart';
import 'package:secret/blocs/auth/auth_event.dart';
import 'package:secret/blocs/auth/auth_state.dart';
import 'package:secret/models/card_model.dart';
import 'package:secret/pages/card/view/card_view_bloc.dart';
import 'package:secret/pages/card/view/card_view_event.dart';
import 'package:secret/pages/card/view/card_view_state.dart';
import 'package:secret/pages/login/login_page.dart';
import 'package:secret/services/activity_service.dart';
import 'package:photo_view/photo_view.dart';
import 'package:share/share.dart';
import 'package:zefyr/zefyr.dart';
import 'package:zefyr/zefyr.dart';
import 'package:zefyr/zefyr.dart';


class CardViewPage extends StatefulWidget {
  final String cardId;
  final bool fromDeepLink;

  CardViewPage({@required this.cardId, this.fromDeepLink=false});

  @override
  State<StatefulWidget> createState() => CardViewPageState(
    cardId: cardId,
  );

}

class CardViewPageState extends State<CardViewPage> {
  final String cardId;
  final CardViewBloc _cardViewBloc;

  bool _onShared = false;

  CardViewPageState({@required this.cardId})
      : _cardViewBloc = CardViewBloc(cardId: cardId);


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
//        if (widget.fromDeepLink && Platform.isAndroid) {
//          await moveTaskToBack();
//          return false;
//        }

        return true;
      },

      child: BlocBuilder<AuthEvent, AuthState>(
          bloc: AuthBloc.instance,
          builder: (BuildContext context, AuthState state) {
            if (state is AuthInitialized) {
              if (state.isAuthenticated) {
                return _buildCardViewPage(context: context);
              }

              _onWidgetDidBuild(() {
                Navigator.of(context)
                    .pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (context) => LoginPage(
                          redirectTo: '/card/${widget.cardId}',
                        )
                    ), (_) => false
                );
              });
            }

            return _buildLoadingPage(context: context);
          }
      ),
    );
  }
  Widget _buildLoadingPage({@required BuildContext context}) {
    return Scaffold(
      appBar: AppBar(
        title: Text('拆信中'),
      ),
      body: Container(
        alignment: Alignment.center,
        child: SpinKitFadingCube (
            color: Theme.of(context).primaryColor,
            size: 40
        ),
      ),
    );
  }

  Widget _buildCardViewPage({@required BuildContext context}) {

    return BlocBuilder<CardViewEvent, CardViewState>(
      bloc: _cardViewBloc,
      builder: (BuildContext context, CardViewState state) {
        if (state.isInitial) {
          _cardViewBloc.dispatch(CardViewRequestEvent(
            fromDeepLink: widget.fromDeepLink,
          ));
          return _buildLoadingPage(context: context);
        }

        if (state.isLoading) {
          return _buildLoadingPage(context: context);
        }

        if (state.hasLoadingError) {
          return _buildLoadingErrorPage(context: context,
              errorMessage: state.loadingError);
        }


        final CardModel cardModel = state.cardModel;

        final String appBarTitle = (cardModel.creator.uid ==
            AuthBloc.instance.currentUser.uid)
            ? '給${cardModel.receiver.name}的一封信'
            : '來自${cardModel.creator.name}的信';


        return Scaffold(
          appBar: AppBar(
            title: Text(appBarTitle),
            actions: <Widget>[
              _buildShareIconButton(
                context: context,
                bloc: _cardViewBloc,
                state: state,
              ),
            ],
          ),
          body: _buildScaffoldBody(
            context: context,
            bloc: _cardViewBloc,
            state: state,
          ),
        );
      }
    );
  }

  Widget _buildLoadingErrorPage({@required BuildContext context, @required String errorMessage}) {
    return Scaffold(
      appBar: AppBar(
        title: Text('這封信'),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Text(
          '出錯囉！\n錯誤: $errorMessage',
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget _buildScaffoldBody({
    @required BuildContext context,
    @required CardViewBloc bloc,
    @required CardViewState state}) {

    return Builder(
        builder: (context) {

          _checkShareStatus(
            context: context,
            bloc: bloc,
            state: state,
          );

          final CardModel _cardModel = state.cardModel;

          return ListView(
            padding: EdgeInsets.symmetric(horizontal: 20),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 20),
                child: Text(
                  _cardModel.title,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 0),
                child: Text(
                  '寫於${_formatTimestamp(_cardModel.createdAt)}',
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.black45,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: ZefyrView(
                  document: _cardModel.body,
                  imageDelegate: new CustomImageDelegate(),
                ),
              )
            ],
          );
        });


  }

  Widget _buildShareIconButton({
    @required BuildContext context,
    @required CardViewBloc bloc,
    @required CardViewState state}) {
    return state.isLoadingDeepLink
        ? Padding(
      padding: EdgeInsets.all(10),
      child: SpinKitWave(
          color: Colors.white,
          size: 40
      ),
    ) : IconButton(
        icon: Icon(Icons.share),
        onPressed: () {
          _shareRequest(
            context: context,
            bloc: bloc,
            state: state,
          );
        }
    );
  }


  void _checkShareStatus({
    @required BuildContext context,
    @required CardViewBloc bloc,
    @required CardViewState state}) {

    if (_onShared && !state.isLoadingDeepLink ) {
      if (state.hasDeepLinkError) {
        _onWidgetDidBuild(() {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(state.deepLinkError),
              backgroundColor: Colors.red,
            ),
          );
        });
      } else {
        _share(state.deepLink);
      }

      _onShared = false;
    }

    if (!_onShared && state.isLoadingDeepLink) {
      _onShared = true;
    }
  }


  void _shareRequest({
    @required BuildContext context,
    @required CardViewBloc bloc,
    @required CardViewState state}) {

    if (!state.hasDeepLinkError && state.deepLink != null) {
      _share(state.deepLink);
      return;
    }

    _cardViewBloc.dispatch(
        CardViewSharingEvent(),
    );
  }

  Future<void> _share(String deepLink) async {
    final RenderBox box = context.findRenderObject();
    await Share.share(deepLink,
        sharePositionOrigin:
        box.localToGlobal(Offset.zero) &
        box.size
    );
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  String _formatTimestamp(Timestamp timestamp) {
    final DateTime dateTime = timestamp.toDate().toLocal();
//    return DateFormat('MMM dd, yyyy h:mm a').format(dateTime);
    return DateFormat('yyyy年M月dd日 H點mm分').format(dateTime);
  }

}

class CustomImageDelegate implements ZefyrImageDelegate {
  @override
  Widget buildImage(BuildContext context, String imageSource) {
    // We use custom "asset" scheme to distinguish asset images from other files.
    if (imageSource.startsWith(RegExp(r'http(s)?://'))) {
      return _buildNetworkImageView(
          context: context,
          imageUrl: imageSource
      );
    } else if (imageSource.startsWith('asset://')) {
      return _buildImageView(
          context: context,
          provider: AssetImage(imageSource.replaceFirst('asset://', '')),
      );
    } else if (imageSource.startsWith('file://')) {
      final file = File.fromUri(Uri.parse(imageSource));
      final image = FileImage(file);

      return _buildImageView(
          context: context,
          provider: image
      );
    }

    return Container();
  }

  Widget _buildNetworkImageView({
    @required BuildContext context,
    @required String imageUrl,
  }) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      elevation: 2,
      child: TransitionToImage(
        image: AdvancedNetworkImage(
          imageUrl,
          useDiskCache: true,
        ),
        fit: BoxFit.contain,
        width: 300,
        loadingWidget: SizedBox(
          height: 300,
          width: 300,
          child: SpinKitPumpingHeart(
            size: 30,
            color: Theme.of(context).primaryColor,
          ),
        ),
        placeholder: SizedBox(
          height: 300,
          width: 300,
          child: Icon(
            Icons.refresh,
            size: 30,
            color: Theme.of(context).primaryColor,
          ),
        ),
        enableRefresh: true,
      ),
    );

  }

  Widget _buildImageView({
    @required BuildContext context,
    @required ImageProvider provider,
  }) {
    return Container(
        alignment: Alignment.center,
        child: Container(
          width: 300,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            elevation: 2,
            child:Image(
              alignment: Alignment.center,
              image: provider,
            ),
          ),
        ),
    );
  }

  @override
  // TODO: implement cameraSource
  get cameraSource => throw UnimplementedError();

  @override
  // TODO: implement gallerySource
  get gallerySource => throw UnimplementedError();

  @override
  Future<String> pickImage(source) {
    // TODO: implement pickImage
    throw UnimplementedError();
  }
}
