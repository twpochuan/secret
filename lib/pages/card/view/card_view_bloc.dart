import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:meta/meta.dart';
import 'package:secret/blocs/auth/auth_bloc.dart';
import 'package:secret/models/card_model.dart';
import 'package:secret/models/person_model.dart';
import 'package:secret/pages/card/view/card_view_event.dart';
import 'package:secret/pages/card/view/card_view_state.dart';
import 'package:secret/services/api_service.dart';
import 'package:secret/utilities/dynamic_link_creator.dart';

class CardViewBloc extends Bloc<CardViewEvent, CardViewState> {
  final String cardId;
  CardModel _cardModel;

  CardViewBloc({@required this.cardId});

  @override
  CardViewState get initialState => CardViewState.initial();

  @override
  Stream<CardViewState> mapEventToState(CardViewState currentState, CardViewEvent event) async* {
    if (event is CardViewRequestEvent) {
      if (currentState.isLoading) {
        return;
      }

      try {
        yield CardViewState.loading();
        _cardModel = await _getCard();
        yield CardViewState.cardModel(cardModel: _cardModel);
      } catch (e) {
        yield CardViewState.loadingFailure(loadingError: e.toString());
      }

      if (event.fromDeepLink) {
        try {
          await _checkAndSaveCard();
        } catch (e) {
          print('Not saved: ${e.toString()}');
        }
      }
    }

    if (event is CardViewSharingEvent) {
      if (!currentState.hasDeepLinkError &&
          currentState.deepLink != null) {
        yield currentState;
      } else {
        yield CardViewState.basedOn(
            previousState: currentState,
            isLoadingDeepLink: true,
        );

        try {
          PersonModel creator = _cardModel.creator;
          PersonModel receiver = _cardModel.receiver;

          String deepLink = await createDynamicLink(
              docId: cardId,
              title: '${creator.name}給${receiver.name}的一封信',
              description: _cardModel.title,
              imageUrl: 'https://firebasestorage.googleapis.com/'
                  'v0/b/dear-961fc.appspot.com/o/deep_link_image.jpg'
                  '?alt=media&token=2464afa2-0b2a-48d7-afad-be0bf114c8fe',
            );

          yield CardViewState.basedOn(
              previousState: currentState,
              deepLink: deepLink
          );

        } catch (e) {
          yield CardViewState.basedOn(
              previousState: currentState,
              deepLinkError: e.toString(),
          );
        }
      }

    }
  }

  Future<CardModel> _getCard() async {
    assert(cardId != null);

    // Create document on /cards/
    DocumentSnapshot cardSnapshot = await Firestore.instance
        .collection('cards').document(cardId).get();

    Map<String, dynamic> data = Map.of(cardSnapshot.data);
    data['id'] = cardSnapshot.documentID;

    List<PersonModel> userList = await Future.wait(
        ['receiverId', 'creatorId'].map((uid) =>
            ApiService.instance.getPerson(data[uid])
        ),
    );

    return CardModel.fromPartialJson(
        data,
        receiver: userList.first,
        creator: userList.last,
    );
  }

  Future<void> _checkAndSaveCard() async {
    assert(cardId != null);
    assert(_cardModel != null);

    FirebaseUser user = AuthBloc.instance.currentUser;
    String uid = user.uid;
    DocumentReference docReference =  Firestore.instance
        .collection('users').document(uid)
        .collection('cards').document(cardId);

    DocumentSnapshot cardRefSnapshot = await docReference.get();

    print('card exists ${cardRefSnapshot.exists}');
    if (!cardRefSnapshot.exists) {
      print('setData...');

      await docReference
          .setData({
        'creatorId': _cardModel.creator.uid,
        'receiverId': _cardModel.receiver.uid,
        'title': _cardModel.title,
        'type': _cardModel.type,
        'createdAt': FieldValue.serverTimestamp(),
      });
    }
  }
}
