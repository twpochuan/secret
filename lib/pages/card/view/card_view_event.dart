import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class CardViewEvent extends Equatable {
  CardViewEvent([Iterable props]) : super(props);
}

class CardViewRequestEvent extends CardViewEvent {
  final bool fromDeepLink;
  CardViewRequestEvent({@required this.fromDeepLink})
      : super([fromDeepLink]);
}
class CardViewSharingEvent extends CardViewEvent {}
