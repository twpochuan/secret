import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

class LoginState extends Equatable {
  final bool isInitial;
  final bool isLoading;
  final bool isDone;
  final String emailFormatError;
  final String passwordFormatError;
  final String error;
  final FirebaseUser user;

  LoginState({
    @required this.isInitial,
    @required this.isLoading,
    @required this.isDone,
    @required this.emailFormatError,
    @required this.passwordFormatError,
    @required this.error,
    @required this.user,
  }) : super([isInitial, isLoading, isDone, emailFormatError, passwordFormatError, error, user]);

  bool get isLoginButtonEnabled {
    return !isInitial
        && !isLoading
        && !isDone
        && emailFormatError.isEmpty
        && passwordFormatError.isEmpty;
  }

  bool get hasError {
    return error.isNotEmpty
        && emailFormatError.isNotEmpty
        && passwordFormatError.isNotEmpty;
  }

  bool get isEmailFormatValid => emailFormatError.isEmpty;
  bool get isPasswordFormatValid => passwordFormatError.isEmpty;

  factory LoginState.initial() {
    return LoginState(
      isInitial: true,
      isLoading: false,
      isDone: false,
      emailFormatError: '',
      passwordFormatError: '',
      error: '',
      user: null,
    );
  }

  factory LoginState.loading() {
    return LoginState(
      isInitial: false,
      isLoading: true,
      isDone: false,
      emailFormatError: '',
      passwordFormatError: '',
      error: '',
      user: null,
    );
  }

  factory LoginState.isTyping() {
    return LoginState(
      isInitial: false,
      isLoading: false,
      isDone: false,
      emailFormatError: '',
      passwordFormatError: '',
      error: '',
      user: null,
    );
  }

  factory LoginState.failure({String emailFormatError: '',
    String passwordFormatError: '', String error: ''}) {
      return LoginState(
        isInitial: false,
        isLoading: false,
        isDone: false,
        emailFormatError: emailFormatError,
        passwordFormatError: passwordFormatError,
        error: error,
        user: null,
    );
  }



  factory LoginState.success({@required FirebaseUser user}) {
    return LoginState(
      isInitial: false,
      isLoading: false,
      isDone: true,
      emailFormatError: '',
      passwordFormatError: '',
      error: '',
      user: user,
    );
  }

  @override
  String toString() =>
      'LoginState { isInitial: $isInitial,'
          'isLoading: $isLoading, '
          'isLoginButtonEnabled: $isLoginButtonEnabled, '
          'emailFormatError: $emailFormatError, '
          'passwordFormatError: $passwordFormatError, '
          'error: $error, '
          'user: $user }';
}