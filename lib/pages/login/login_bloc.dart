import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:secret/blocs/auth/auth_bloc.dart';
import 'package:secret/blocs/auth/auth_event.dart';
import 'package:secret/pages/login/login_event.dart';
import 'package:secret/pages/login/login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final FirebaseAuth firebaseAuth;
  final RegExp _emailRegExp =  RegExp(r'.+@[^.]+[.].+');
  final AuthBloc _authBloc;


  @override
  get initialState => LoginState.initial();

  LoginBloc()
      : firebaseAuth = FirebaseAuth.instance,
        _authBloc = AuthBloc.instance;

  @override
  Stream<LoginState> mapEventToState(currentState, event) async* {
    if (event is LoginEmailOnChangedEvent) {
      yield LoginState(
        isInitial: false,
        isLoading: currentState.isLoading,
        isDone: false,
        emailFormatError: '',
        passwordFormatError: currentState.passwordFormatError,
        error: '',
        user: currentState.user,
      );
    }

    if (event is LoginPasswordOnChangedEvent) {
      yield LoginState(
        isInitial: false,
        isLoading: currentState.isLoading,
        isDone: false,
        emailFormatError: currentState.emailFormatError,
        passwordFormatError: '',
        error: '',
        user: currentState.user,
      );
    }

    if (event is LoginRequestEvent) {
      bool hasError = false;
      String emailFormatErrorMsg = '';
      String passwordFormatErrorMsg = '';


      if (event.email == null || event.email.isEmpty) {
        hasError = true;
        emailFormatErrorMsg = '看門犬：信箱不能是空白的.';
      } else if (!_isEmailValid(event.email)) {
        hasError = true;
        emailFormatErrorMsg = '看門犬：老兄，你的信箱格式不太對吧';
      }

      if (event.password == null || event.password.isEmpty) {
        hasError = true;
        passwordFormatErrorMsg = '看門犬：你的通關密語呢？';
      } else if (!_isPasswordValid(event.password)) {
        hasError = true;
        passwordFormatErrorMsg = '看門犬：通關密語好像沒這麼短吧？';
      }

      if (hasError) {
        yield LoginState.failure(
            emailFormatError: emailFormatErrorMsg,
            passwordFormatError: passwordFormatErrorMsg
        );
      } else {
        yield LoginState.loading();

        try {
          await firebaseAuth.signInWithEmailAndPassword(
              email: event.email,
              password: event.password);
          _authBloc.dispatch(StatusUpdatedAuthEvent());
        } catch (e) {
          yield LoginState.failure(error: e.toString());
        }
      }
    } else if (event is LoggedInEvent) {
      yield LoginState.initial();
    }
  }

  bool _isEmailValid(String email) {
    return email != null && _emailRegExp.hasMatch(email);
  }

  bool _isPasswordValid(String password) {
    return password != null && password.length >= 6;
  }


}
