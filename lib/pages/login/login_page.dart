import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:secret/blocs/auth/auth_bloc.dart';
import 'package:secret/blocs/auth/auth_event.dart';
import 'package:secret/blocs/auth/auth_state.dart';
import 'package:secret/pages/login/login_bloc.dart';
import 'package:secret/pages/login/login_event.dart';
import 'package:secret/pages/login/login_state.dart';
import 'package:secret/config/application.dart';
import 'package:secret/config/routes.dart';

class LoginPage extends StatefulWidget {
  final String redirectTo ;

  LoginPage({redirectTo})
      : this.redirectTo = redirectTo ?? Routes.root;



  @override
  State<StatefulWidget> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final String _title = '先過我這關';
  final LoginBloc _loginBloc = LoginBloc();
  final TextEditingController _emailTextController = TextEditingController();
  final TextEditingController _passwordTextController = TextEditingController();
  String _lastEmail;
  String _lastPassword;

  bool _loginSucceeded(AuthState state) {
    return state is AuthInitialized && state.isAuthenticated;
  }

  bool _loginFailed(LoginState state) => state.error.isNotEmpty;


  LoginPageState() {
    _loadEmailAndPassword();

    _emailTextController.addListener(() {
      String email = _emailTextController.text;
      if (_lastEmail != email) {
        _loginBloc.dispatch(LoginEmailOnChangedEvent());
        _lastEmail = email;
      }
    });

    _passwordTextController.addListener(() {
      String password = _emailTextController.text;
      if (_lastPassword != password) {
        _loginBloc.dispatch(LoginPasswordOnChangedEvent());
        _lastPassword = password;
      }

    });
  }

  Future<void> _loadEmailAndPassword() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _emailTextController.text = prefs.getString('email') ?? '';
    _passwordTextController.text = prefs.getString('password') ?? '';
  }

  Future<void> _saveEmailAndPassword() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('email', _emailTextController.text);
    await prefs.setString('password', _passwordTextController.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Icon(
                Icons.security,
            ),
            SizedBox(
              width: 10,
            ),
            Text(_title),
          ],
        ),
      ),
      body: BlocBuilder<AuthEvent, AuthState>(
        bloc: AuthBloc.instance,
        builder: (BuildContext context, AuthState state) {
          if (_loginSucceeded(state)) {
            _saveEmailAndPassword();
            _onWidgetDidBuild(() {
              Application.instance.router.navigateTo(
                  context,
                  widget.redirectTo,
                  clearStack: true
              );
            });
          }

          return _buildLoginPage(context: context);
        },
      ),
    );
  }

  Widget _buildLoginPage({@required BuildContext context}) {
    return BlocBuilder<LoginEvent, LoginState>(
        bloc: _loginBloc,
        builder: (BuildContext context, LoginState state) {
          if (_loginFailed(state)) {
            _onWidgetDidBuild(() {
              print('error!!!!!!!');
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('闖關失敗！'
                      '\n看門犬：想過我這關哪有這麼容易！'
                      '\n回去確認你的信箱和密碼再過來吧！'),
//                  content: Text('Cannot log in.\nCheck your email and password again.'),
                  backgroundColor: Colors.red,
                ),
              );
            });
          }

          return _buildForm(
              context: context,
              bloc: _loginBloc,
              state: state
          );
        }
    );
  }

  Widget _buildForm({
    @required BuildContext context,
    @required LoginBloc bloc,
    @required LoginState state}) {
    return Form(
      child: ListView(
        padding: EdgeInsets.symmetric(horizontal:20 ,vertical: 10),
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 93, right: 10, top: 20),
            child: Image(
              alignment: Alignment.center,
              image: AssetImage('assets/login.png'),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 20),
            child: _buildEmailTextFormField(
                context: context,
                bloc: bloc,
                state: state
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: _buildPasswordTextFormField(
                context: context,
                bloc: bloc,
                state: state
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: _buildSubmitButton(
                context: context,
                bloc: bloc,
                state: state
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildEmailTextFormField({
    @required BuildContext context,
    @required LoginBloc bloc,
    @required LoginState state}) {
    return SizedBox(
      height: 55,
      child: TextFormField(
        enabled: !state.isLoading,
        keyboardType: TextInputType.emailAddress,
        controller: _emailTextController,
        decoration: InputDecoration(
          icon: Icon(
            Icons.mail,
            color: Colors.grey,
          ),
          hintText: 'you@example.com',
          labelText: '信箱',
          errorText: errorText(state.emailFormatError),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(16.0)),
        ),
      ),
    );
  }

  Widget _buildPasswordTextFormField({
    @required BuildContext context,
    @required LoginBloc bloc,
    @required LoginState state}) {

    return SizedBox(
        height: 55,
        child: TextFormField(
          enabled: !state.isLoading,
          keyboardType: TextInputType.text,
          controller: _passwordTextController,
          obscureText: true,
          decoration: InputDecoration(
            icon: Icon(
              Icons.lock,
              color: Colors.grey,
            ),
            hintText: '通關密語',
            labelText: '密碼',
            errorText: errorText(state.passwordFormatError),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(16.0)),
          ),
        )
    );
  }

  Widget _buildSubmitButton({
    @required BuildContext context,
    @required LoginBloc bloc,
    @required LoginState state}) {

    return SizedBox(
      height: 55,
      child:RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        child: _buildSubmitButtonChild(
            state: state
        ),
        color: Theme.of(context).primaryColor,
        onPressed: _submitLogin,
      ),
    );
  }

  void _submitLogin() {
    _loginBloc.dispatch(
        LoginRequestEvent(
            email: _emailTextController.text,
            password: _passwordTextController.text
        )
    );
  }

  Widget _buildSubmitButtonChild({@required LoginState state}) {
    return state.isLoading
        ? CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white))
        : Text(
      '闖關',
      style: TextStyle(color: Colors.white, fontSize: 18.0),
    );
  }

  String errorText(String errorMessage) {
    return errorMessage.isNotEmpty ? errorMessage : null;
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }
}