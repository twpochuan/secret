import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class LoginEvent extends Equatable {
  LoginEvent([Iterable props]) : super(props);
}

class LoginEmailOnChangedEvent extends LoginEvent {}
class LoginPasswordOnChangedEvent extends LoginEvent {}

class LoginRequestEvent extends LoginEvent {
  final String email;
  final String password;

  LoginRequestEvent({
    @required this.email,
    @required this.password
  }) :super([email, password]);
}

class LoggedInEvent extends LoginEvent {}