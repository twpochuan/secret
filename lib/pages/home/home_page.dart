import 'dart:async';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:fluro/fluro.dart';


import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:secret/blocs/auth/auth_bloc.dart';
import 'package:secret/blocs/auth/auth_event.dart';
import 'package:secret/blocs/auth/auth_state.dart';
import 'package:secret/config/application.dart';
import 'package:secret/models/card_ref_model.dart';
import 'package:secret/models/person_model.dart';
import 'package:secret/pages/card/creation/card_creation_page.dart';
import 'package:secret/pages/card/view/card_view_page.dart';
import 'package:secret/pages/home/home_bloc.dart';
import 'package:secret/pages/home/home_event.dart';
import 'package:secret/pages/home/home_state.dart';
import 'package:secret/services/api_service.dart';
import 'package:intl/intl.dart';



enum UniLinksType { string, uri }

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() =>
      HomePageState();

}


class HomePageState extends State<HomePage> {
  final String _title = '我們的窩';
  final HomeBloc _homeBloc = HomeBloc();
  final double _cardHeight = 300;
  final double _cardWidth = 180;

  StreamSubscription _sub;
  UniLinksType _type = UniLinksType.string;


  @override
  void initState() {
    super.initState();
		_initDynamicLinks();
  }

  @override
  void dispose() {
    if (_sub != null) {
      _sub.cancel();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthEvent, AuthState>(
      bloc: AuthBloc.instance,
      builder: (BuildContext context, AuthState state) {
        if (state is AuthInitialized) {
          if (state.isAuthenticated) {
            return _buildHomePage(context: context);
          }
          _onWidgetDidBuild(() {
            Application.instance.router.navigateTo(
              context, 'login',
              clearStack: true,
            );
          });
        }

        return _buildLoadingPage(context: context);
      }
    );
  }

  Widget _buildLoadingPage({@required BuildContext context}) {
    return Scaffold(
        appBar: AppBar(
        title: Row(
          children: <Widget>[
            Icon(
                Icons.home
            ),
            SizedBox(
              width: 10,
            ),
            Text(_title),
          ],
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        child: SpinKitFadingCube (
            color: Theme.of(context).primaryColor,
            size: 40
        ),
      ),
    );

  }


  Widget _buildHomePage({@required BuildContext context}) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Icon(
              Icons.home
            ),
            SizedBox(
              width: 10,
            ),
            Text(_title),
          ],
        ),
        actions: <Widget>[
          PopupMenuButton(
            onSelected: (_) {
              AuthBloc.instance.dispatch(LogoutAuthEvent());
            },
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(
                  value: 0,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.exit_to_app,
                        color: Theme.of(context).primaryColor,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        '離家出走',
                      ),
                    ],
                  ),
                ),
              ];
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: '寫點什麼',
        child: Icon(Icons.add),
        onPressed: () async {
          PersonModel receiver = await ApiService.instance.getContact();
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => CardCreationPage(receiver: receiver),
            ),
          );
        },
      ),
      body: _buildHomePageBody(context: context),
    );
  }

  Widget _buildHomePageBody({@required BuildContext context}) {
    return BlocBuilder<HomeEvent, HomeState>(
      bloc: _homeBloc,
      builder: (BuildContext context, HomeState state) {
        if (state.isInitial) {
          if (!state.isLoading) {
            _homeBloc.dispatch(HomeFirstRequestEvent());
          }

          return _buildLoadingBody(context: context);
        }

        if (state.items == null && state.hasError) {
          return _buildLoadingErrorBody(
              context: context,
              errorMessage: state.error
          );
        }
        return Container(
          alignment: Alignment.topCenter,
          child: ListView(
            padding: EdgeInsets.only(top: 90),
//            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 160,
                child: Image(
                  alignment: Alignment.topCenter,
                  image: AssetImage('assets/home.gif'),
                ),
              ),
              Container(
                height: 200,
                child: _buildListView(
                  context: context,
                  bloc: _homeBloc,
                  state: state,
                ),
              ),

            ],
          ),

        );
      },
    );

  }


  Widget _buildListView({
    @required BuildContext context,
    @required HomeBloc bloc,
    @required HomeState state,
  }) {
    final List<CardRefModel> items = state.items;
    final double leftPaddingSize =
        MediaQuery.of(context).size.width * 0.25;



    return ListView.builder(
        padding: EdgeInsets.only(left: leftPaddingSize),
        scrollDirection: Axis.horizontal,
        itemBuilder:  _cardItemBuilder(
          context: context,
          bloc: _homeBloc,
          state: state,
        ),
        itemCount: state.hasNext
            ? items.length + 3
            : max(items.length, 1),
    );
  }




  Widget _buildLoadingErrorBody({
    @required BuildContext context,
    @required String errorMessage,
  }) {
    return Container(
      alignment: Alignment.center,
      child: Text(
        '出錯囉！\n錯誤: $errorMessage',
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildLoadingBody({@required BuildContext context}) {
    return Container(
      alignment: Alignment.center,
      child: SpinKitFadingCube (
          color: Theme.of(context).primaryColor,
          size: 40
      ),
    );
  }

  IndexedWidgetBuilder _cardItemBuilder({
    @required BuildContext context,
    @required HomeBloc bloc,
    @required HomeState state,
  }) {


    assert(!state.isInitial);
    assert(state.items != null);
    List<CardRefModel> items = state.items;

    if (items.length == 0) {
      return (BuildContext context, int index) {
        return _buildEmptyCard(context: context);
      };
    }

    return (BuildContext context, int index) {
      if (index >= items.length) {
        if (!state.isLoading) {
          bloc.dispatch(
              HomeRequestNextBatchEvent(
                  afterTimestamp: items.last.createdAt
              )
          );
        }
        return _buildLoadingCard(
          context: context,
        );
      }

      return _buildCardItem(
          context: context,
          cardRefModel: items[index]
      );

    };
  }


  Widget _buildCardItem({
    @required BuildContext context,
    @required CardRefModel cardRefModel
  }) {


    return SizedBox(
      height: _cardHeight,
      width: _cardWidth,
      child: Card(
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Theme.of(context).primaryColor,
            ),
            borderRadius: BorderRadius.circular(8.0),
          ),
          margin: EdgeInsets.all(10),
          elevation: 3,
          child: ListTile(
            title: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 6),
                  alignment: Alignment.topLeft,
                  child: Text(
                    '寄信人: ${cardRefModel.creator.name}',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    '收信人: ${cardRefModel.receiver.name}',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
                Container(
                  height: 110,
                  alignment: Alignment.center,
                  child: Text(
                    cardRefModel.title,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ),
                Text(
                  '收信: ${_formatTimestamp(cardRefModel.createdAt)}',
                  style: TextStyle(
                    color: Colors.black45,
                    fontSize: 11,
                  ),
                ),
              ],
            ),
            onTap: () {
              Application.instance.router.navigateTo(
                context,
                '/card/${cardRefModel.id}',
                transition: TransitionType.fadeIn,
              );
            },
            onLongPress: () {

            },
          ),
      ),
    );
  }



  Widget _buildLoadingCard({
    @required BuildContext context,
  }) {
    return SizedBox(
      height: _cardHeight,
      width: _cardWidth,
      child: Card(
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: Theme.of(context).primaryColor,
          ),
          borderRadius: BorderRadius.circular(8.0),
        ),
        margin: EdgeInsets.all(10),
        elevation: 3,
        child: SpinKitFadingCube(
          color: Theme.of(context).primaryColor,
          size: 40,
        ),
      ),
    );
  }

  Widget _buildEmptyCard({
    @required BuildContext context,
  }) {
    return SizedBox(
        height: _cardHeight,
        width: _cardWidth,
        child: Card(
            shape: RoundedRectangleBorder(
              side: BorderSide(
                color: Theme.of(context).primaryColor,
              ),
              borderRadius: BorderRadius.circular(8.0),
            ),
            margin: EdgeInsets.all(10),
            elevation: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('等待第一封信的到來'),
              ],
            )
        ),
    );
  }

  String _formatTimestamp(Timestamp timestamp) {
    final DateTime dateTime = timestamp.toDate().toLocal();
//    return DateFormat('MMM dd, yyyy h:mm a').format(dateTime);
        return DateFormat('M月dd日 H點mm分').format(dateTime);
  }


  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }


  void _checkDeepLinkAndNavigate(Uri deepLink) {
		assert(deepLink != null);
		final String cardId = deepLink.queryParameters['card'];
		if (cardId != null) {
			_navigateToCardView(cardId);
		}
	}

	void _initDynamicLinks() async {
		final PendingDynamicLinkData data = await FirebaseDynamicLinks.instance.getInitialLink();
		final Uri deepLink = data?.link;

		if (deepLink != null) {
			_checkDeepLinkAndNavigate(deepLink);
		}

		FirebaseDynamicLinks.instance.onLink(
			onSuccess: (PendingDynamicLinkData dynamicLink) async {
				final Uri deepLink = dynamicLink?.link;

				if (deepLink != null) {
					_checkDeepLinkAndNavigate(deepLink);
				}
			},
			onError: (OnLinkErrorException e) async {
				print('onLinkError');
				print(e.message);
			}
		);
	}

  void _navigateToCardView(String cardId) {
  	assert(cardId != null);
		Navigator.pushAndRemoveUntil(
			context,
			PageRouteBuilder(
				pageBuilder: (context, animation1, animation2) {
					return CardViewPage(
						cardId: cardId,
						fromDeepLink: true,
					);
				},
				transitionDuration: Duration(milliseconds: 0),
			),
			ModalRoute.withName('/'),
		);
	}


}

