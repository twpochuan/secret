import 'dart:async';
import 'dart:collection';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';
import 'package:secret/blocs/auth/auth_bloc.dart';
import 'package:secret/models/card_ref_model.dart';
import 'package:secret/models/person_model.dart';
import 'package:secret/pages/home/home_event.dart';
import 'package:secret/pages/home/home_state.dart';
import 'package:secret/services/api_service.dart';
import 'package:secret/utilities/memory_cache.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final int batchSize = 20;
  List<CardRefModel> _items = <CardRefModel>[];
  StreamSubscription<QuerySnapshot> _cardRefSub;

  @override
  HomeState get initialState => HomeState.initial();

  @override
  Stream<HomeState> mapEventToState(HomeState currentState,
      HomeEvent event) async* {

    if (event is HomeNewItemNotifyEvent) {


      yield HomeState.basedOn(
        previousState: currentState,
        items: UnmodifiableListView<CardRefModel>(_items),
        error: null,
      );

      return;
    }

    yield HomeState.basedOn(
      previousState: currentState,
      isLoading: true,
      error: null,
    );

    try {
      List<CardRefModel> newItems;
      if (event is HomeFirstRequestEvent) {
        newItems = await _getBatchCardRefs();
        Timestamp receivedAfter = (newItems.isNotEmpty)
            ? newItems.first?.createdAt
            : null;
        _setupCardRefListener(
          receivedAfter: receivedAfter,
        );


      } else if (event is HomeRequestNextBatchEvent) {
        newItems = await _getBatchCardRefs(receivedBefore: event.afterTimestamp);
      }

      _items.addAll(newItems);


      yield HomeState.basedOn(
        previousState: currentState,
        isInitial: false,
        isLoading: false,
        items: UnmodifiableListView<CardRefModel>(_items),
        error: null,
        hasNext: newItems.length >= batchSize,
      );
    } catch (e) {
      yield HomeState.basedOn(
        previousState: currentState,
        isInitial: false,
        isLoading: false,
        error: e.toString(),
      );
    }

  }

  void _setupCardRefListener({@required Timestamp receivedAfter}) {
    if (_cardRefSub != null) {
      _cardRefSub.cancel();
    }

    FirebaseUser currentUser = AuthBloc.instance.currentUser;
    String uid = currentUser.uid;
    Stream<QuerySnapshot> streamSnapshot = Firestore.instance.collection('users')
        .document(uid).collection('cards').orderBy('createdAt', descending: true)
        .endBefore([receivedAfter]).limit(1).snapshots();

    _cardRefSub = streamSnapshot.listen((querySnapshot) async {

      List<DocumentSnapshot> docSnapshots = querySnapshot?.documents;

      if (docSnapshots.isEmpty) {
        return;
      }

      DocumentSnapshot docSnapshot = docSnapshots.first;

      Map<String, dynamic> data = Map.of(docSnapshot.data);
      data['id'] = docSnapshot.documentID;

      if (_isCardRefDocFromServer(data)) {
        _items.insert(0, await _convertToCardRefModel(data));
        dispatch(HomeNewItemNotifyEvent());
      }
    });
  }

  bool _isCardRefDocFromServer(Map<String, dynamic> data) {
    return data['createdAt'] != null;
  }

  @override
  void dispose() {
    if (_cardRefSub != null) {
      _cardRefSub.cancel();
    }

    super.dispose();
  }

  Future<List<CardRefModel>> _getBatchCardRefs({Timestamp receivedBefore}) async {
    FirebaseUser currentUser = AuthBloc.instance.currentUser;
    String uid = currentUser.uid;

    Query query = Firestore.instance.collection('users')
        .document(uid).collection('cards')
        .orderBy('createdAt', descending: true);
    
    if (receivedBefore != null) {
      query = query.startAfter([receivedBefore]);
    }


    QuerySnapshot querySnapshot = await query.limit(batchSize).getDocuments();


    List<CardRefModel> list = <CardRefModel>[];

    for (DocumentSnapshot docSnapshot in querySnapshot.documents) {
      Map<String, dynamic> data = Map.of(docSnapshot.data);
      data['id'] = docSnapshot.documentID;
      list.add(await _convertToCardRefModel(data));
    }

    return list;
  }

  Future<CardRefModel> _convertToCardRefModel(Map<String, dynamic> data) async {
    List<PersonModel> userList = await Future.wait(
      ['receiverId', 'creatorId'].map((uid) =>
          ApiService.instance.getPerson(data[uid])
      ),
    );

    return CardRefModel.fromPartialJson(data,
        receiver: userList.first,
        creator: userList.last
    );
  }




}
