import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class HomeEvent extends Equatable {
  HomeEvent([Iterable props]) : super(props);
}

class HomeFirstRequestEvent extends HomeEvent {}

class HomeRequestNextBatchEvent extends HomeEvent {
  final Timestamp afterTimestamp;
  HomeRequestNextBatchEvent({@required this.afterTimestamp})
      : super([afterTimestamp]);
}

class HomeNewItemNotifyEvent extends HomeEvent {}