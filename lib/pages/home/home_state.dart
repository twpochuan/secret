import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:secret/models/card_ref_model.dart';

class HomeState extends Equatable {
  final bool isInitial;
  final bool isLoading;
  final List<CardRefModel> items;
  final bool hasNext;
  final String error;

  HomeState({
    @required this.isInitial,
    @required this.isLoading,
    @required this.items,
    @required this.hasNext,
    @required this.error
  }): super([isInitial, isLoading, items, hasNext, error]);



  bool get hasError => error != null
      && error.isNotEmpty;

  factory HomeState.initial() {
    return HomeState(
      isInitial: true,
      isLoading: false,
      items: null,
      hasNext: true,
      error: null,
    );
  }


  factory HomeState.basedOn({
    @required HomeState previousState,
    bool isInitial,
    bool isLoading,
    List<CardRefModel> items,
    bool hasNext,
    String error
  }) {
    return HomeState(
      isInitial: isInitial ?? previousState.isInitial,
      isLoading: isLoading ?? previousState.isLoading,
      items: items ?? previousState.items,
      hasNext: hasNext ?? previousState.hasNext,
      error: error ?? previousState.error,
    );
  }
}