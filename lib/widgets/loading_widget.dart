import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  final double height;
  final double width;

  LoadingWidget({this.height, this.width});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: height,
      width: width,
      child: Image(
          image: AssetImage('assets/loading.gif')
      ),
    );
  }

}