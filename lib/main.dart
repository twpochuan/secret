
import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:secret/config/application.dart';
import 'package:flutter_crashlytics/flutter_crashlytics.dart';




void main() async {
  bool isInDebugMode = false;

//  FlutterError.onError = (FlutterErrorDetails details) {
//    if (isInDebugMode) {
//      // In development mode simply print to console.
//      FlutterError.dumpErrorToConsole(details);
//    } else {
//      // In production mode report to the application zone to report to
//      // Crashlytics.
//      Zone.current.handleUncaughtError(details.exception, details.stack);
//    }
//  };
//
//  await FlutterCrashlytics().initialize();
//  Firestore.instance.settings(timestampsInSnapshotsEnabled: true);



  runApp(MaterialApp(
    title: '我們的窩',
    theme: ThemeData(
        primaryColor: Colors.deepOrange,
        primaryColorLight: Colors.deepOrangeAccent,
        accentColor: Colors.deepOrangeAccent,
    ),
    onGenerateRoute: Application.instance.router.generator,
  ));
}
